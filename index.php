<?php
setlocale(LC_MONETARY, 'en_US');
include_once 'lib/inventory.php';
include_once 'lib/currency.php';
include_once 'lib/validation.php';

$inventory = new Inventory();
$currency = new Currency();
$validation = new Validation();
?>
<!DOCTYPE html>
<!-- saved from url=(0057)https://v4-alpha.getbootstrap.com/examples/sticky-footer/ -->
<html lang="en" class="gr__v4-alpha_getbootstrap_com">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="https://v4-alpha.getbootstrap.com/favicon.ico">
        <title>Audit Search</title>
        <!-- Bootstrap core CSS -->
        <link href="./css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="./css/sticky-footer.css" rel="stylesheet">
        <link href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/css/bootstrap.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    </head>
    <body data-gr-c-s-loaded="true" style="">
        <!-- Begin page content -->
        <div class="container">
            <div class="mt-1">
                <h1 class="text-center">Car Search (Audit)</h1>
            </div>
            <div><br /><br /></div>
            <div>
                <form method="post" class="form-inline">
                    <label class="col-lg-2 text-left">Search by ZIP Code </label>
                    <input name="zipcode" value="<?php echo isset($_POST['zipcode']) ? $_POST['zipcode'] : ''; ?>" type="text" class="form-control col-lg-3" placeholder="For example: 1960">
                    <label class="col-lg-2">Distance (miles)</label>
                    <select name="distance" class="form-control col-lg-3">
                        <option>Select</option>
                        <?php foreach (range(1, 100) as $value): ?>
                            <option <?php if (isset($_POST['distance']) && $_POST['distance'] == $value): ?>selected=""<?php endif; ?> value="<?php echo $value; ?>"><?php echo $value; ?> miles</option>
                        <?php endforeach; ?>
                    </select>
                    &nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;
                    <button type="submit" class="btn btn-primary col-lg-1">Search</button>
                </form>

                <br />
                <?php $results = []; ?>
                <?php if (isset($_POST['zipcode'])) : ?>
                    <?php if ($validation->validate_zipcode($_POST['zipcode'])): ?>
                        <?php $results = $inventory->search($_POST); ?>
                    <?php else: ?>
                        <div class="alert alert-danger" role="alert">
                            Error! invalid zip code
                        </div>
                    <?php endif; ?>
                <?php endif; ?>

                <?php if (count($results)): ?>
                    <table id="inventory" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Year</th>
                                <th>Make</th>
                                <th>Model</th>
                                <th>Price</th>
                                <th>Dealership</th>
                                <th>Location (state, city, zip code)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($results as $result): ?>
                                <?php $result = (object) $result; ?>
                                <tr>
                                    <th scope="row"><?php echo $result->year; ?></th>
                                    <td><?php echo $result->make; ?></td>
                                    <td><?php echo $result->model; ?></td>
                                    <td><?php echo $currency->money_format('%.2n', $result->price); ?></td>
                                    <td><?php echo $result->dealer_name; ?></td>
                                    <td>
                                        <?php echo $result->county_name; ?>,
                                        <?php echo $result->city; ?>,
                                        <?php echo $result->zip_code; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php else: ?>
                    <?php if (count($_POST)): ?>
                        <?php if ($gen_utility->validate_zipcode($_POST['zipcode'])): ?>
                            <div class="alert alert-danger" role="alert">
                                Note! Search records unavailable
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>

        <div class="row">
            <br /><br /><br /><br /><br />
        </div>

        <footer class="footer">
            <div class="container">
                <span class="text-muted">Copyright © 2017</span>
            </div>
        </footer>

        <script src="//code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#inventory').DataTable();
            });
        </script>
    </body>
</html>
