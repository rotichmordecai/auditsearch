<?php

/*
 *
 * -------------------------------------------------------
 * Class name:        location
 * Creation date:  04.10.2017
 * -------------------------------------------------------
 */

// **********************
// Class 
// **********************

class location extends Database {

    // **********************
    // Attribute 
    // **********************

    private $zip_code;   // Key Attribute
    private $dealer_number;   // DataType: int(11)
    private $dealer_name;   // DataType: varchar(49)
    private $address_1;   // DataType: varchar(38)
    private $ppc_phone;   // DataType: int(11)
    private $pricing_tier;   // DataType: varchar(1)
    private $ppc_extension;   // DataType: int(11)

    // **********************
    // Constructor
    // **********************

    function __construct() {
        parent::__construct();
    }

    // **********************
    // Getter
    // **********************

    function get_dealer_number() {
        return $this->dealer_number;
    }

    function get_dealer_name() {
        return $this->dealer_name;
    }

    function get_address_1() {
        return $this->address_1;
    }

    function get_ppc_phone() {
        return $this->ppc_phone;
    }

    function get_pricing_tier() {
        return $this->pricing_tier;
    }

    function get_ppc_extension() {
        return $this->ppc_extension;
    }

    // **********************
    // Setter
    // **********************

    function set_dealer_number($value) {
        $this->dealer_number = $value;
    }

    function set_dealer_name($value) {
        $this->dealer_name = $value;
    }

    function set_address_1($value) {
        $this->address_1 = $value;
    }

    function set_ppc_phone($value) {
        $this->ppc_phone = $value;
    }

    function set_pricing_tier($value) {
        $this->pricing_tier = $value;
    }

    function set_ppc_extension($value) {
        $this->ppc_extension = $value;
    }

    /**
     * Initialize location
     * @param type $row
     */
    function init($row) {
        $this->dealer_number = $row->dealer_number;
        $this->dealer_name = $row->dealer_name;
        $this->address_1 = $row->address_1;
        $this->ppc_phone = $row->ppc_phone;
        $this->pricing_tier = $row->pricing_tier;
        $this->ppc_extension = $row->ppc_extension;
    }

    /**
     * Select location
     * 
     * @param type $criteria
     */
    public function select($criteria = null) {
        
    }

    /**
     * Get location by zip_code
     * @param type $id
     */
    public function get_dealer($id) {
        
    }

    /**
     * Delete location
     * @param type $criteria
     */
    public function delete($criteria = null) {
        
    }

    /**
     * Create location
     * @param type $param
     */
    public function create($param) {
        
    }

    /**
     * Update location
     * @param type $param
     */
    public function update($param) {
        
    }

}

?>