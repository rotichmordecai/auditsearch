<?php

/*
 *
 * -------------------------------------------------------
 * Class name:     Dealer
 * Creation date:  04.10.2017
 * -------------------------------------------------------
 */

// **********************
// Class 
// **********************

class Dealer extends Database {

    // **********************
    // Attribute 
    // **********************

    private $dealer_number;   // Key Attribute
    private $dealer_name;   // DataType: varchar(49)
    private $address_1;   // DataType: varchar(38)
    private $ppc_phone;   // DataType: int(11)
    private $pricing_tier;   // DataType: varchar(1)
    private $ppc_extension;   // DataType: int(11)

    // **********************
    // Constructor 
    // **********************

    public function __construct() {
        parent::__construct();
    }

    // **********************
    // Getter 
    // **********************

    public function get_dealer_number() {
        return $this->dealer_number;
    }

    public function get_dealer_name() {
        return $this->dealer_name;
    }

    public function get_address_1() {
        return $this->address_1;
    }

    public function get_ppc_phone() {
        return $this->ppc_phone;
    }

    public function get_pricing_tier() {
        return $this->pricing_tier;
    }

    public function get_ppc_extension() {
        return $this->ppc_extension;
    }

    // **********************
    // Setter 
    // **********************

    public function set_dealer_number($value) {
        $this->dealer_number = $value;
    }

    public function set_dealer_name($value) {
        $this->dealer_name = $value;
    }

    public function set_address_1($value) {
        $this->address_1 = $value;
    }

    public function set_ppc_phone($value) {
        $this->ppc_phone = $value;
    }

    public function set_pricing_tier($value) {
        $this->pricing_tier = $value;
    }

    public function set_ppc_extension($value) {
        $this->ppc_extension = $value;
    }

    /**
     * Initialize dealer
     * @param type $row
     */
    public function init($row) {
        $this->dealer_number = $row->dealer_number;
        $this->dealer_name = $row->dealer_name;
        $this->address_1 = $row->address_1;
        $this->ppc_phone = $row->ppc_phone;
        $this->pricing_tier = $row->pricing_tier;
        $this->ppc_extension = $row->ppc_extension;
    }

    /**
     * Select dealer
     * 
     * @param type $criteria
     */
    public function select($criteria = null) {
        
    }

    /**
     * Get dealer by dealer number
     * @param type $id
     */
    public function get_dealer($id) {
        
    }

    /**
     * Delete dealer
     * @param type $criteria
     */
    public function delete($criteria = null) {
        
    }

    /**
     * Create dealer
     * @param type $param
     */
    public function create($param) {
        
    }

    /**
     * Update dealer
     * @param type $param
     */
    public function update($param) {
        
    }

}
