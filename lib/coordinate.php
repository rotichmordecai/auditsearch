<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Coordinate
 *
 * @author Admin
 */
class Coordinate {

    public function __construct() {
        ;
    }

    /**
     * 
     * @param type $lat1
     * @param type $lon1
     * @param type $lat2
     * @param type $lon2
     * @param type $unit
     * @return type
     */
    function Distance($lat1, $lon1, $lat2, $lon2, $unit) {

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }

    /**
     * Distance is in miles, alat and alon are in degrees
     * 
     * @param type $alat
     * @param type $alon
     * @param type $distance
     * @param type $bearing
     * @return type
     */
    function CalculateCoordinate($alat, $alon, $distance, $bearing) {

        $pi = 3.14159265358979;
        $alatRad = $alat * $pi / 180;
        $alonRad = $alon * $pi / 180;
        $bearing = $bearing * $pi / 180;
        $alatRadSin = sin($alatRad);
        $alatRadCos = cos($alatRad);
        // Ratio of distance to earth's radius
        $angularDistance = $distance / 3963;
        $angDistSin = sin($angularDistance);
        $angDistCos = cos($angularDistance);
        $xlatRad = asin($alatRadSin * $angDistCos +
                $alatRadCos * $angDistSin * cos($bearing));
        $xlonRad = $alonRad + atan2(sin($bearing) * $angDistSin * $alatRadCos, $angDistCos - $alatRadSin * sin($xlatRad));
        // Return latitude and longitude as two element array in degrees
        $xlat = $xlatRad * 180 / $pi;
        $xlon = $xlonRad * 180 / $pi;

        if ($xlat > 90)
            $xlat = 90;
        if ($xlat < -90)
            $xlat = -90;
        while ($xlat > 180)
            $xlat -= 360;
        while ($xlat <= -180)
            $xlat += 360;
        while ($xlon > 180)
            $xlon -= 360;
        while ($xlon <= -180)
            $xlon += 360;
        return array($xlat, $xlon);
    }

    /**
     * Distance is in km, lat and lon are in degrees
     * 
     * @param type $lat
     * @param type $lon
     * @param type $distance
     * @return type
     */
    function SquareAround($lat, $lon, $distance) {
        return array(
            $this->CalculateCoordinate($lat, $lon, $distance, 0), // Get north point
            $this->CalculateCoordinate($lat, $lon, $distance, 90), // Get east point
            $this->CalculateCoordinate($lat, $lon, $distance, 180), // Get south point
            $this->CalculateCoordinate($lat, $lon, $distance, 270) // Get west point
        );
    }

    /**
     * Returns array containing an array with min lat, max lat, min lon, max lon
     * If the square defining these points crosses the 180-degree meridian, two
     * such arrays are returned.  Otherwise, one such array (within another array)
     * is returned.
     * 
     * Example: Gets extreme coordinates around point at (10.0,20.0)
     * print_r(SquareAround(10.0, 180, 100));
     * print_r(CoordinateMinMax(10.0, 180, 100));
     * 
     * @param type $lat
     * @param type $lon
     * @param type $distance
     * @return type
     */
    function CoordinateMinMax($lat, $lon, $distance) {
        $s = $this->SquareAround($lat, $lon, $distance);
        if ($s[3][1] > $s[1][1]) {// if west longitude is greater than south longitude
            // Crossed the 180-degree meridian
            return array(
                array($s[2][0], $s[0][0], $s[3][1], 180),
                array($s[2][0], $s[0][0], -180, $s[1][1])
            );
        } else {
            // Didn't cross the 180-degree meridian (usual case)
            return array(
                array($s[2][0], $s[0][0], $s[3][1], $s[1][1])
            );
        }
    }

}
