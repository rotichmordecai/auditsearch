<?php

require_once 'database.php';

require_once 'validation.php';
require_once 'coordinate.php';

class Inventory extends Database {

    public $inventory; // inventory
    
    private $validation; //geo utility
    private $coordinate; //general utility
    // **********************
    // Attribute Declaration
    // **********************
    private $id;   // Key Attribute
    private $vin;   // DataType: varchar(150)
    private $stock;   // DataType: varchar(150)
    private $make;   // DataType: varchar(150)
    private $model;   // DataType: varchar(150)
    private $trim;   // DataType: varchar(50)
    private $year;   // DataType: int(11)
    private $amenities;   // DataType: varchar(150)
    private $price;   // DataType: int(11)
    private $miles;   // DataType: int(11)
    private $interior;   // DataType: varchar(150)
    private $description;   // DataType: varchar(150)
    private $certified;   // DataType: tinyint(4)
    private $transmission;   // DataType: varchar(25)
    private $bodytype;   // DataType: varchar(25)
    private $speed;   // DataType: varchar(25)
    private $doors;   // DataType: int(11)
    private $cylinders;   // DataType: int(11)
    private $engine;   // DataType: varchar(25)
    private $displacement;   // DataType: int(11)
    private $zip_code;   // DataType: int(11)
    private $phone;   // DataType: varchar(10)
    private $imagefile;   // DataType: varchar(250)
    private $dealer_number;   // DataType: int(11)
    private $distance;   // DataType: int(11)
    var $hm;

    // **********************
    // Constructor 
    // **********************

    public function __construct() {
        parent::__construct();
        $this->validation = new Validation();
        $this->coordinate = new Coordinate();
    }

    // **********************
    // Getter 
    // **********************

    function get_id() {
        return $this->id;
    }

    function get_vin() {
        return $this->vin;
    }

    function get_stock() {
        return $this->stock;
    }

    function get_make() {
        return $this->make;
    }

    function get_model() {
        return $this->model;
    }

    function get_trim() {
        return $this->trim;
    }

    function get_year() {
        return $this->year;
    }

    function get_amenities() {
        return $this->amenities;
    }

    function get_price() {
        return $this->price;
    }

    function get_miles() {
        return $this->miles;
    }

    function get_interior() {
        return $this->interior;
    }

    function get_description() {
        return $this->description;
    }

    function get_certified() {
        return $this->certified;
    }

    function get_transmission() {
        return $this->transmission;
    }

    function get_bodytype() {
        return $this->bodytype;
    }

    function get_speed() {
        return $this->speed;
    }

    function get_doors() {
        return $this->doors;
    }

    function get_cylinders() {
        return $this->cylinders;
    }

    function get_engine() {
        return $this->engine;
    }

    function get_displacement() {
        return $this->displacement;
    }

    function get_zip_code() {
        return $this->zip_code;
    }

    function get_phone() {
        return $this->phone;
    }

    function get_imagefile() {
        return $this->imagefile;
    }

    function get_dealer_number() {
        return $this->dealer_number;
    }

    function get_distance() {
        return $this->distance;
    }

    // **********************
    // Setter 
    // **********************

    function set_id($value) {
        $this->id = $value;
    }

    function set_vin($value) {
        $this->vin = $value;
    }

    function set_stock($value) {
        $this->stock = $value;
    }

    function set_make($value) {
        $this->make = $value;
    }

    function set_model($value) {
        $this->model = $value;
    }

    function set_trim($value) {
        $this->trim = $value;
    }

    function set_year($value) {
        $this->year = $value;
    }

    function set_amenities($value) {
        $this->amenities = $value;
    }

    function set_price($value) {
        $this->price = $value;
    }

    function set_miles($value) {
        $this->miles = $value;
    }

    function set_interior($value) {
        $this->interior = $value;
    }

    function set_description($value) {
        $this->description = $value;
    }

    function set_certified($value) {
        $this->certified = $value;
    }

    function set_transmission($value) {
        $this->transmission = $value;
    }

    function set_bodytype($value) {
        $this->bodytype = $value;
    }

    function set_speed($value) {
        $this->speed = $value;
    }

    function set_doors($value) {
        $this->doors = $value;
    }

    function set_cylinders($value) {
        $this->cylinders = $value;
    }

    function set_engine($value) {
        $this->engine = $value;
    }

    function set_displacement($value) {
        $this->displacement = $value;
    }

    function set_zip_code($value) {
        $this->zip_code = $value;
    }

    function set_phone($value) {
        $this->phone = $value;
    }

    function set_imagefile($value) {
        $this->imagefile = $value;
    }

    function set_dealer_number($value) {
        $this->dealer_number = $value;
    }

    function set_distance($value) {
        $this->distance = $value;
    }

    /**
     * Initialize inventory
     * @param type $row
     */
    function init($row) {
        $this->id = $row->id;
        $this->vin = $row->vin;
        $this->stock = $row->stock;
        $this->make = $row->make;
        $this->model = $row->model;
        $this->trim = $row->trim;
        $this->year = $row->year;
        $this->amenities = $row->amenities;
        $this->price = $row->price;
        $this->miles = $row->miles;
        $this->interior = $row->interior;
        $this->description = $row->description;
        $this->certified = $row->certified;
        $this->transmission = $row->transmission;
        $this->bodytype = $row->bodytype;
        $this->speed = $row->speed;
        $this->doors = $row->doors;
        $this->cylinders = $row->cylinders;
        $this->engine = $row->engine;
        $this->displacement = $row->displacement;
        $this->zip_code = $row->zip_code;
        $this->phone = $row->phone;
        $this->imagefile = $row->imagefile;
        $this->dealer_number = $row->dealer_number;
        $this->distance = $row->distance;
    }

    /**
     * Search for inventory within a selected distance from a defined zip code
     * 
     * @param type $input
     * @return type
     */
    public function Search($input) {

        // If valid search
        if (isset($input['zipcode']) && isset($input['distance'])) {

            // clean up input
            $zipcode = htmlspecialchars($input['zipcode']);
            $distance = htmlspecialchars($input['distance']);

            if ($this->validation->validate_zipcode($zipcode)) {

                // search for zip location
                $records = ORM::for_table('location')
                        ->where(array(
                            'zip_code' => $zipcode
                        ))
                        ->find_many();

                // if location record available 
                if (count($records) == 1) {
                    foreach ($records as $record) {
                        $minmax = $this->coordinate->CoordinateMinMax($record['latitude'], $record['longitude'], $distance);
                        $raw_sql = "SELECT inventory.*, dealer.*, location.* FROM inventory JOIN dealer ON dealer.dealer_number = inventory.dealer_number JOIN location ON location.zip_code = inventory.zip_code WHERE (location.latitude BETWEEN " . $minmax[0][0] . " AND " . $minmax[0][1] . ") AND (location.longitude BETWEEN " . $minmax[0][2] . " AND " . $minmax[0][3] . ") AND Distance('mi', " . $record['latitude'] . ", " . $record['longitude'] . ", location.latitude, location.longitude) <= $distance";
                        $pdo = ORM::get_db();
                        $this->inventory = $pdo->query($raw_sql);
                    }
                }
            }
        }
        return $this->inventory;
    }

    /**
     * Select inventory
     * 
     * @param type $criteria
     */
    public function select($criteria = null) {
        
    }

    /**
     * Get inventory by zip_code
     * @param type $id
     */
    public function get_dealer($id) {
        
    }

    /**
     * Delete inventory
     * @param type $criteria
     */
    public function delete($criteria = null) {
        
    }

    /**
     * Create inventory
     * @param type $param
     */
    public function create($param) {
        
    }

    /**
     * Update inventory
     * @param type $param
     */
    public function update($param) {
        
    }

}
